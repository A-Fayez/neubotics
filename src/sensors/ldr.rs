use bevy::prelude::*;
use bevy_prototype_lyon::prelude::*;
use bevy_rapier2d::rapier::geometry::{ColliderBuilder, InteractionGroups};

#[derive(Debug, Default)]
pub struct Ldr(pub super::SensorCollisionSet);

#[derive(Debug)]
pub struct Led {
    on_color: Handle<ColorMaterial>,
    off_color: Handle<ColorMaterial>,
}

pub fn spawn(
    parent: &mut ChildBuilder,
    x: f32,
    y: f32,
    on_color: Handle<ColorMaterial>,
    off_color: Handle<ColorMaterial>,
    meshes: &mut ResMut<Assets<Mesh>>,
    collision_group: InteractionGroups,
) {
    let radius = 5.0;
    let entity = parent
        .spawn(primitive(
            off_color.clone(),
            meshes,
            ShapeType::Circle(radius),
            TessellationMode::Fill(&FillOptions::default()),
            Vec3::new(x, y, 0.4),
        ))
        .with(Ldr::default())
        .with(Led {
            on_color,
            off_color,
        })
        .current_entity()
        .unwrap();

    parent.with(
        ColliderBuilder::ball(radius)
            .translation(x, y)
            .sensor(true)
            .user_data(entity.to_bits() as u128)
            .collision_groups(collision_group),
    );
}

pub fn color(mut sensor_query: Query<(&Ldr, &Led, &mut Handle<ColorMaterial>), Changed<Ldr>>) {
    for (sensor, led, mut material) in sensor_query.iter_mut() {
        *material = if sensor.0.is_colliding() {
            led.on_color.clone()
        } else {
            led.off_color.clone()
        };
    }
}
