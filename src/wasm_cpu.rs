use anyhow::Result;
use bevy::{
    asset::{AssetLoader, LoadContext, LoadedAsset},
    prelude::*,
    reflect::TypeUuid,
    utils::BoxedFuture,
};

struct ModuleHandle {
    name: String,
    handle: Handle<Module>,
    has_loaded: bool,
}

#[derive(Default)]
struct ModuleHandles {
    modules: Vec<ModuleHandle>,
}

impl ModuleHandles {
    pub fn add_module(&mut self, name: impl Into<String>, module_handle: Handle<Module>) {
        self.modules.push(ModuleHandle {
            name: name.into(),
            handle: module_handle,
            has_loaded: false,
        });
    }

    pub fn get_next_unloaded(&mut self) -> Option<&mut ModuleHandle> {
        self.modules.iter_mut().find(|m| m.has_loaded)
    }

    pub fn reset(&mut self) {
        for module in self.modules.iter_mut() {
            module.has_loaded = false;
        }
    }
}

pub struct CpuPlugin;

impl Plugin for CpuPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_asset::<Module>()
            .init_asset_loader::<ModuleLoader>()
            .add_startup_system(setup.system())
            .add_system(run_interpreters.system());
    }
}

#[derive(Clone, Default)]
pub struct ModuleLoader;

#[derive(Debug, Clone, TypeUuid)]
#[uuid = "d4bd6b78-108f-4644-8265-d8b894984127"]
pub struct Module(wasm::Module);

impl AssetLoader for ModuleLoader {
    fn load<'a>(
        &'a self,
        bytes: &'a [u8],
        load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, Result<()>> {
        Box::pin(async move {
            let mut bytes = bytes;
            let module = wasm::Module::from_reader(&mut bytes)?;
            load_context.set_default_asset(LoadedAsset::new(Module(module)));
            Ok(())
        })
    }

    fn extensions(&self) -> &[&str] {
        &["wasm"]
    }
}

fn setup(commands: &mut Commands, asset_server: Res<AssetServer>) {
    let interpreter = wasm::Interpreter::default();
    let mut modules = ModuleHandles::default();
    modules.add_module("wheel", asset_server.load("wheel.wasm"));
    modules.add_module("car1", asset_server.load("car1.wasm"));

    commands.spawn((interpreter, modules));
}

fn run_interpreters(
    time: Res<Time>,
    module_assets: Res<Assets<Module>>,
    mut query: Query<(&mut ModuleHandles, &mut wasm::Interpreter)>,
) {
    for (mut modules, mut interpreter) in query.iter_mut() {
        if !interpreter.is_running() {
            match modules.get_next_unloaded() {
                Some(module_handle) => {
                    if let Some(module) = module_assets.get(&module_handle.handle) {
                        interpreter.load_module(module_handle.name.clone(), &module.0);
                    }
                }
                None => {
                    modules.reset();
                    interpreter.reset();
                }
            }
        }

        if interpreter.is_running() {
            interpreter.advance(100);
        }
    }
}
