use bevy::{prelude::*, render::camera::Camera, window::WindowResized};
use bevy_prototype_lyon::prelude::*;
use bevy_rapier2d::{
    na::Vector2,
    physics::{EventQueue, RapierConfiguration, RapierPhysicsPlugin},
    rapier::{
        dynamics::RigidBodyBuilder,
        geometry::{ColliderBuilder, ColliderSet, InteractionGroups},
    },
};
use std::cmp::min;

mod player;
mod sensors;
mod wasm_cpu;

const FLOOR_COLLISION_GROUP: u16 = 0b01;
const SOLID_COLLISION_GROUP: u16 = 0b10;

#[derive(Debug)]
pub struct Line;
#[derive(Debug)]
pub struct Wall;

#[bevy_main]
fn main() {
    App::build()
        .add_resource(Msaa::default())
        .add_plugins(DefaultPlugins)
        //.add_plugin(bevy::winit::WinitPlugin::default())
        //.add_plugin(bevy::wgpu::WgpuPlugin::default())
        .add_plugin(RapierPhysicsPlugin)
        //.add_plugin(bevy_rapier_2d::render::RapierRenderPlugin)
        .add_resource(RapierConfiguration {
            gravity: Vector2::zeros(),
            ..Default::default()
        })
        .add_resource(ClearColor(Color::rgb(0.9, 0.9, 0.9)))
        .add_startup_system(setup.system())
        .add_system(window_resize.system())
        .add_system(player::movement.system())
        .add_system(collision_events.system())
        .add_system(sensors::ldr::color.system())
        .add_system(sensors::switch::color.system())
        .run();
}

pub fn collision_events(
    colliders: Res<ColliderSet>,
    events: Res<EventQueue>,
    mut ldr_query: Query<&mut sensors::ldr::Ldr>,
    mut switch_query: Query<&mut sensors::switch::Switch>,
) {
    while let Ok(proximity_event) = events.proximity_events.pop() {
        let collider_1 = colliders.get(proximity_event.collider1).unwrap();
        let collider_2 = colliders.get(proximity_event.collider2).unwrap();

        if collider_1.user_data == 0 || collider_2.user_data == 0 {
            continue;
        }

        let entity1 = Entity::from_bits(collider_1.user_data as u64);
        let entity2 = Entity::from_bits(collider_2.user_data as u64);

        ldr_query
            .get_mut(entity1)
            .map(|mut sensor| sensor.0.update(entity2, proximity_event.new_status))
            .ok();
        ldr_query
            .get_mut(entity2)
            .map(|mut sensor| sensor.0.update(entity1, proximity_event.new_status))
            .ok();
        switch_query
            .get_mut(entity1)
            .map(|mut sensor| sensor.0.update(entity2, proximity_event.new_status))
            .ok();
        switch_query
            .get_mut(entity2)
            .map(|mut sensor| sensor.0.update(entity1, proximity_event.new_status))
            .ok();
    }
}

fn camera_transform(width: usize, height: usize) -> Transform {
    let scale = 2000.0 / min(width, height) as f32;
    Transform::from_translation(Vec3::new(0.0, 0.0, 1.0))
        * Transform::from_scale(Vec3::new(scale, scale, scale))
}

fn window_resize(
    resize_event: Res<Events<WindowResized>>,
    mut camera_query: Query<(&Camera, &mut Transform)>,
) {
    let mut reader = resize_event.get_reader();
    if let Some(e) = reader.iter(&resize_event).last() {
        debug!("Window resized: width = {} height = {}", e.width, e.height);
        for (_, mut transform) in camera_query.iter_mut() {
            *transform = camera_transform(e.width as usize, e.height as usize);
        }
    }
}

fn setup(
    commands: &mut Commands,
    windows: Res<Windows>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    let line_color = materials.add(Color::rgba(0.0, 0.0, 0.0, 1.0).into());
    let wall_color = materials.add(Color::rgba(0.5, 0.5, 0.5, 1.0).into());

    let floor_collision_group: InteractionGroups =
        InteractionGroups::new(FLOOR_COLLISION_GROUP, FLOOR_COLLISION_GROUP);
    let solid_collision_group: InteractionGroups =
        InteractionGroups::new(SOLID_COLLISION_GROUP, SOLID_COLLISION_GROUP);

    commands
        .spawn(Camera2dBundle {
            transform: camera_transform(
                windows.get_primary().unwrap().width() as usize,
                windows.get_primary().unwrap().height() as usize,
            ),
            ..Default::default()
        })
        .spawn(LightBundle {
            transform: Transform::from_translation(Vec3::new(1000.0, 100.0, 2000.0)),
            ..Default::default()
        });

    spawn_walls(commands, wall_color, solid_collision_group);
    spawn_line(commands, line_color.clone(), floor_collision_group);
    spawn_ball_line(commands, line_color, &mut meshes, floor_collision_group);

    player::spawn_car1(
        commands,
        &mut materials,
        &mut meshes,
        solid_collision_group,
        floor_collision_group,
        solid_collision_group,
    );
}

use std::f32::consts::TAU;

fn spawn_walls(
    commands: &mut Commands,
    material: Handle<ColorMaterial>,
    collision_group: InteractionGroups,
) {
    let height = 50.0;
    let width = 1500.0;

    let mut spawn_wall = |x, y, rot| {
        let entity = commands
            .spawn(SpriteBundle {
                material: material.clone(),
                transform: Transform::identity(),
                sprite: Sprite::new(Vec2::new(width, height)),
                ..Default::default()
            })
            .with(
                RigidBodyBuilder::new_static()
                    .translation(x, y)
                    .rotation(rot),
            )
            .with(Wall)
            .current_entity()
            .unwrap();

        commands.with_children(|parent| {
            parent.spawn((ColliderBuilder::cuboid(width / 2.0, height / 2.0)
                .user_data(entity.to_bits() as u128)
                .collision_groups(collision_group),));
        });
    };

    let offset = width / 2.0 - height / 2.0;

    spawn_wall(0.0, offset, 0.0);
    spawn_wall(offset, 0.0, 0.25 * TAU);
    spawn_wall(0.0, -offset, 0.5 * TAU);
    spawn_wall(-offset, 0.0, 0.75 * TAU);
}

fn spawn_ball_line(
    commands: &mut Commands,
    material: Handle<ColorMaterial>,
    meshes: &mut ResMut<Assets<Mesh>>,
    collision_group: InteractionGroups,
) {
    let entity = commands
        .spawn(primitive(
            material,
            meshes,
            ShapeType::Circle(100.0),
            TessellationMode::Fill(&FillOptions::default()),
            Vec3::zero(),
        ))
        .with(RigidBodyBuilder::new_static().translation(300.0, 300.0))
        .with(Line)
        .current_entity()
        .unwrap();

    commands.with_children(|parent| {
        parent.spawn((ColliderBuilder::ball(100.0)
            .translation(0.0, 0.0)
            .sensor(true)
            .user_data(entity.to_bits() as u128)
            .collision_groups(collision_group),));
    });
}

fn spawn_line(
    commands: &mut Commands,
    material: Handle<ColorMaterial>,
    collision_group: InteractionGroups,
) {
    let length = 1000.0;
    let width = 40.0;
    let entity = commands
        .spawn(SpriteBundle {
            material,
            transform: Transform::from_translation(Vec3::new(0.0, 0.0, 0.0)),
            sprite: Sprite::new(Vec2::new(length, width)),
            ..Default::default()
        })
        .with(
            RigidBodyBuilder::new_static()
                .translation(0.0, 0.0)
                .rotation(TAU / 8.0),
        )
        .with(Line)
        .current_entity()
        .unwrap();

    commands.with_children(|parent| {
        parent.spawn((ColliderBuilder::cuboid(length / 2.0, width / 2.0)
            .sensor(true)
            .user_data(entity.to_bits() as u128)
            .collision_groups(collision_group),));
    });
}
