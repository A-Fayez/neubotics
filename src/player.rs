use bevy::prelude::*;
use bevy_prototype_lyon::prelude::*;
use bevy_rapier2d::{
    physics::RigidBodyHandleComponent,
    rapier::{
        dynamics::{RigidBodyBuilder, RigidBodySet},
        geometry::{ColliderBuilder, InteractionGroups},
        math::Vector,
    },
};

use crate::sensors::{ldr, switch};

#[derive(Debug)]
pub struct Player;

pub fn spawn_car1(
    commands: &mut Commands,
    materials: &mut ResMut<Assets<ColorMaterial>>,
    meshes: &mut ResMut<Assets<Mesh>>,
    player_collision_group: InteractionGroups,
    ldr_collision_group: InteractionGroups,
    switch_collision_group: InteractionGroups,
) {
    let on_color = materials.add(Color::rgba(1.0, 0.0, 0.0, 1.0).into());
    let off_color = materials.add(Color::rgba(0.6, 0.0, 0.0, 0.2).into());
    commands
        .spawn(SpriteBundle {
            material: materials.add(Color::rgba(0.0, 0.0, 0.0, 0.0).into()),
            sprite: Sprite::new(Vec2::new(200.0, 100.0)),
            ..Default::default()
        })
        .with(RigidBodyBuilder::new_dynamic().translation(-400.0, 0.0))
        .with(Player)
        .with_children(|parent| {
            parent.spawn((ColliderBuilder::cuboid(100.0, 50.0)
                .density(1.0)
                .collision_groups(player_collision_group),));
            parent.spawn(primitive(
                materials.add(Color::rgb(0.3, 0.5, 0.2).into()),
                meshes,
                ShapeType::RoundedRectangle {
                    width: 200.0,
                    height: 100.0,
                    border_radius: 15.0,
                },
                TessellationMode::Fill(&FillOptions::default()),
                Vec3::new(-200.0 / 2.0, -100.0 / 2.0, 0.1),
            ));

            ldr::spawn(
                parent,
                95.0,
                45.0,
                on_color.clone(),
                off_color.clone(),
                meshes,
                ldr_collision_group,
            );
            ldr::spawn(
                parent,
                95.0,
                20.0,
                on_color.clone(),
                off_color.clone(),
                meshes,
                ldr_collision_group,
            );
            ldr::spawn(
                parent,
                95.0,
                -20.0,
                on_color.clone(),
                off_color.clone(),
                meshes,
                ldr_collision_group,
            );
            ldr::spawn(
                parent,
                95.0,
                -45.0,
                on_color.clone(),
                off_color.clone(),
                meshes,
                ldr_collision_group,
            );

            switch::spawn(
                parent,
                95.0,
                0.0,
                on_color.clone(),
                off_color.clone(),
                meshes,
                switch_collision_group,
            )
        });
}

pub fn movement(
    keyboard_input: Res<Input<KeyCode>>,
    mut bodies: ResMut<RigidBodySet>,
    mut player_query: Query<(&Player, &RigidBodyHandleComponent)>,
) {
    let angular_speed = 2.0;
    let speed = Vector::new(300.0, 0.0);

    for (_, body_handle) in player_query.iter_mut() {
        let body = bodies.get_mut(body_handle.handle()).unwrap();

        if keyboard_input.pressed(KeyCode::W) {
            body.set_linvel(body.position().rotation * speed, true)
        } else if keyboard_input.pressed(KeyCode::S) {
            body.set_linvel(body.position().rotation * -speed, true)
        } else {
            body.set_linvel(Vector::new(0.0, 0.0), false)
        }

        if keyboard_input.pressed(KeyCode::A) {
            body.set_angvel(angular_speed, true);
        } else if keyboard_input.pressed(KeyCode::D) {
            body.set_angvel(-angular_speed, true);
        } else {
            body.set_angvel(0.0, false);
        }
    }
}
