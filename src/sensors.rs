use bevy::prelude::*;
use bevy_rapier2d::rapier::ncollide::query::Proximity;
use std::collections::HashSet;

pub mod ldr;
pub mod switch;

#[derive(Debug, Default)]
pub struct SensorCollisionSet {
    colliding_entities: HashSet<Entity>,
}

impl SensorCollisionSet {
    pub fn is_colliding(&self) -> bool {
        !self.colliding_entities.is_empty()
    }

    pub fn add_entity(&mut self, entity: Entity) {
        self.colliding_entities.insert(entity);
    }

    pub fn remove_entity(&mut self, entity: &Entity) {
        self.colliding_entities.remove(entity);
    }

    pub fn update(&mut self, entity: Entity, status: Proximity) {
        if status == Proximity::Intersecting {
            self.add_entity(entity);
        } else {
            self.remove_entity(&entity);
        }
    }
}
