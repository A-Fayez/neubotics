use wasm::{self, Interpreter};
use wast::{
    parser::{self, ParseBuffer},
    Wast, WastDirective,
};

use std::{fs::read_to_string, path::Path};

pub fn run(file: impl AsRef<Path>) {
    let file = file.as_ref();

    let wast = &read_to_string(file).expect("could not read test file");
    let buf = ParseBuffer::new(&wast).expect("could not create parser");
    let tests = parser::parse::<Wast>(&buf).expect("could not parse the test file");

    let mut interpreter = Interpreter::default();
    let mut last_name = "".to_string();

    for directive in tests.directives {
        match directive {
            WastDirective::Module(mut module) => {
                println!(
                    "\n=== Loading module on line {}",
                    line_no(&module.span, &wast)
                );

                last_name = module.id.map(|n| n.name().to_string()).unwrap_or_default();
                let wasm = module.encode().expect("could not encode wat module");
                let module = wasm::Module::from_reader(&mut &wasm[..]).expect("Invalid module");
                interpreter.load_module(last_name.clone(), &module);
                interpreter.advance(10);
            }
            WastDirective::QuoteModule { span, source } => {
                let (_, _) = (span, source);
                dbg!("quote module");
            }
            WastDirective::AssertMalformed {
                span,
                module,
                message,
            } => {
                let (_, _, _) = (span, module, message);
                dbg!("assert malformed");
            }
            WastDirective::AssertInvalid {
                span,
                module,
                message,
            } => {
                let (_, _, _) = (span, module, message);
                dbg!("assert invalid");
            }
            WastDirective::Register { span, name, module } => {
                let (_, _, _) = (span, name, module);
                dbg!("register");
            }
            WastDirective::Invoke(invoke) => {
                invoke_fn(&last_name, &mut interpreter, invoke);
            }
            WastDirective::AssertTrap {
                span,
                exec,
                message,
            } => {
                let (_, _, _) = (span, exec, message);
                dbg!("assert trap");
            }
            WastDirective::AssertReturn {
                span,
                exec,
                results,
            } => {
                println!(
                    "\n=== Running assert return on line {}",
                    line_no(&span, &wast)
                );

                match exec {
                    wast::WastExecute::Invoke(invoke) => {
                        let module_name = invoke
                            .module
                            .map(|id| id.name())
                            .unwrap_or(last_name.as_str());
                        let actual = invoke_fn(module_name, &mut interpreter, invoke);
                        assert_eq!(actual.len(), results.len());
                        for (expected, actual) in results.iter().zip(actual.iter()) {
                            match expected {
                                wast::AssertExpression::I32(i) => {
                                    assert_eq!(actual.to_i32(), Some(*i))
                                }
                                wast::AssertExpression::I64(i) => {
                                    assert_eq!(actual.to_i64(), Some(*i))
                                }
                                wast::AssertExpression::F32(i) => {
                                    assert_eq!(
                                        actual.to_f32().map(f32::to_bits),
                                        Some(to_inner(i).bits)
                                    )
                                }
                                wast::AssertExpression::F64(i) => {
                                    assert_eq!(
                                        actual.to_f64().map(f64::to_bits),
                                        Some(to_inner(i).bits)
                                    )
                                }
                                wast::AssertExpression::V128(_) => unimplemented!("v128"),
                                wast::AssertExpression::RefNull(_) => unimplemented!("refnull"),
                                wast::AssertExpression::RefExtern(_) => unimplemented!("refextern"),
                                wast::AssertExpression::RefFunc(_) => unimplemented!("reffunc"),
                                wast::AssertExpression::LegacyArithmeticNaN => {
                                    unimplemented!("legacyarithemticnan")
                                }
                                wast::AssertExpression::LegacyCanonicalNaN => match actual {
                                    wasm::Value::F32(f) => assert!(f.is_nan()),
                                    wasm::Value::F64(f) => assert!(f.is_nan()),
                                    v => panic!("expected nan got {:?}", v),
                                },
                            }
                        }
                    }
                    wast::WastExecute::Module(module) => {
                        let _ = module;
                        dbg!("invoke module");
                    }
                    wast::WastExecute::Get { module, global } => {
                        dbg!("invoke get", module, global);
                    }
                }
            }
            WastDirective::AssertExhaustion {
                span,
                call,
                message,
            } => {
                let (_, _, _) = (span, call, message);
                dbg!("assert exhaustion");
            }
            WastDirective::AssertUnlinkable {
                span,
                module,
                message,
            } => {
                let (_, _, _) = (span, module, message);
                dbg!("assert unlinkable");
            }
        }
    }
}

fn invoke_fn<'i>(
    module_name: &str,
    interpreter: &'i mut Interpreter,
    invoke: wast::WastInvoke,
) -> &'i [wasm::Value] {
    let parameters = invoke
        .args
        .iter()
        .map(expression_to_value)
        .collect::<Vec<_>>();
    interpreter
        .call(module_name, invoke.name, &parameters)
        .unwrap();
    interpreter.advance(5_000_000);
    assert!(
        !interpreter.is_running(),
        "interpreter is still running with test",
    );
    interpreter.results().expect("function not yet complete")
}

fn expression_to_value(expression: &wast::Expression) -> wasm::Value {
    assert_eq!(expression.instrs.len(), 1);
    match &expression.instrs[0] {
        wast::Instruction::I32Const(v) => wasm::Value::I32(*v),
        wast::Instruction::I64Const(v) => wasm::Value::I64(*v),
        wast::Instruction::F32Const(v) => wasm::Value::F32(f32::from_bits(v.bits)),
        wast::Instruction::F64Const(v) => wasm::Value::F64(f64::from_bits(v.bits)),
        instruction => panic!("non-const instruction: {:?}", instruction),
    }
}

fn to_inner<T>(value: &wast::NanPattern<T>) -> &T {
    match value {
        wast::NanPattern::ArithmeticNan => unimplemented!(),
        wast::NanPattern::CanonicalNan => unimplemented!(),
        wast::NanPattern::Value(t) => t,
    }
}

fn line_no(span: &wast::Span, wast: &str) -> usize {
    let (line, _) = span.linecol_in(wast);
    line + 1
}
