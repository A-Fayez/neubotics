pub(crate) use crate::structure::instructions::Instr;
use crate::types;
use parity_wasm::elements::{self, Deserialize};
use std::{collections::HashMap, fs, io, path::Path};
use thiserror::Error;

#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub(crate) struct TypeIndex(pub usize);
#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub(crate) struct FunctionIndex(pub usize);
#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub(crate) struct TableIndex(pub usize);
#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub(crate) struct MemoryIndex(pub usize);
#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub(crate) struct GlobalIndex(pub usize);
#[derive(Debug, Default, PartialEq, PartialOrd, Clone, Copy)]
pub(crate) struct InstructionIndex(pub usize);

#[derive(Debug, Clone)]
pub(crate) struct Function {
    pub type_index: TypeIndex,
    pub locals: Vec<types::Local>,
    pub code: Vec<Instr>,
}

#[derive(Debug, Clone)]
pub(crate) struct Table {
    pub initial: u32,
    pub max: Option<u32>,
    pub offset: Option<Vec<ConstantInstruction>>,
    pub members: Vec<FunctionIndex>,
}

#[derive(Debug, Clone)]
pub(crate) struct Memory {
    pub initial: u32,
    pub max: Option<u32>,
    pub values: Vec<MemoryData>,
}

#[derive(Debug, Clone)]
pub(crate) struct MemoryData {
    pub offset: Option<Vec<ConstantInstruction>>,
    pub data: Vec<u8>,
}

#[derive(Debug, Clone)]
pub(crate) struct Global {
    pub value_type: types::ValType,
    pub is_mutable: bool,
    pub init: Vec<ConstantInstruction>,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub(crate) enum Exports {
    Function(FunctionIndex),
    Table(TableIndex),
    Memory(MemoryIndex),
    Global(GlobalIndex),
}

#[derive(Debug, Clone)]
pub struct Module {
    pub(crate) types: Vec<types::FuncType>,
    pub(crate) functions: Vec<Function>,
    pub(crate) tables: Vec<Table>,
    pub(crate) memory: Vec<Memory>,
    pub(crate) globals: Vec<Global>,
    pub(crate) exports: HashMap<String, Exports>,
}

impl InstructionIndex {
    pub fn increment(&mut self) {
        self.0 += 1;
    }
}

impl Function {
    pub(crate) fn instruction(&self, index: InstructionIndex) -> Instr {
        self.code
            .get(index.0)
            .expect("no more instructions")
            .clone()
    }
}

impl Module {
    pub fn from_file(path: impl AsRef<Path>) -> Result<Result<Module, ModuleError>, io::Error> {
        Ok(Self::from_reader(&mut fs::File::open(path)?))
    }

    pub fn from_reader<R: io::Read>(reader: &mut R) -> Result<Module, ModuleError> {
        let parity_module = parity_wasm::elements::Module::deserialize(reader).unwrap();

        if parity_module.version() != 1 {
            return Err(ModuleError::UnsupportedVersion(parity_module.version()));
        }

        let types = if let Some(types) = parity_module.type_section() {
            types.types().iter().map(From::from).collect()
        } else {
            Vec::new()
        };

        let functions = match (
            parity_module.function_section(),
            parity_module.code_section(),
        ) {
            (Some(functions), Some(code)) => functions
                .entries()
                .iter()
                .zip(code.bodies().iter())
                .map(|(function, code)| {
                    let type_index = TypeIndex(function.type_ref() as usize);
                    assert!(
                        type_index.0 < types.len(),
                        "type_index is out of range got {}, types length {}",
                        type_index.0,
                        types.len()
                    );
                    let locals = code
                        .locals()
                        .iter()
                        .flat_map(|local| {
                            (0..local.count()).map(move |_| local.value_type().into())
                        })
                        .collect();
                    let code = code.code().elements().iter().map(From::from).collect();
                    Function {
                        type_index,
                        locals,
                        code,
                    }
                })
                .collect(),
            (None, None) => Vec::new(),
            (Some(function), None) if function.entries().len() == 0 => Vec::new(),
            (None, Some(code)) if code.bodies().len() == 0 => Vec::new(),
            _ => unimplemented!(),
        };

        let exports = if let Some(export_secton) = parity_module.export_section() {
            export_secton
                .entries()
                .iter()
                .map(|export| (export.field().to_string(), export.internal().into()))
                .collect()
        } else {
            HashMap::new()
        };

        let mut tables = if let Some(table_section) = parity_module.table_section() {
            table_section
                .entries()
                .iter()
                .map(|table| {
                    let limits = table.limits();
                    Table {
                        initial: limits.initial(),
                        max: limits.maximum(),
                        offset: None,
                        members: Vec::new(),
                    }
                })
                .collect()
        } else {
            Vec::new()
        };

        if let Some(element_section) = parity_module.elements_section() {
            for element in element_section.entries() {
                let table = &mut tables[element.index() as usize];
                table.offset = element
                    .offset()
                    .as_ref()
                    .map(|offset| offset.code().iter().map(From::from).collect());
                table.members = element
                    .members()
                    .iter()
                    .map(|&m| FunctionIndex(m as usize))
                    .collect();
            }
        }

        let mut memory = if let Some(memory_section) = parity_module.memory_section() {
            memory_section
                .entries()
                .iter()
                .map(|memory| {
                    let limits = memory.limits();
                    Memory {
                        initial: limits.initial(),
                        max: limits.maximum(),
                        values: Vec::new(),
                    }
                })
                .collect()
        } else {
            Vec::new()
        };

        if let Some(data_section) = parity_module.data_section() {
            for data_segment in data_section.entries() {
                let memory = &mut memory[data_segment.index() as usize];
                memory.values.push(MemoryData {
                    offset: data_segment
                        .offset()
                        .as_ref()
                        .map(|offset| offset.code().iter().map(From::from).collect()),
                    data: data_segment.value().to_vec(),
                })
            }
        }

        let globals = if let Some(global_section) = parity_module.global_section() {
            global_section
                .entries()
                .iter()
                .map(|global| {
                    let global_type = global.global_type();
                    Global {
                        value_type: global_type.content_type().into(),
                        is_mutable: global_type.is_mutable(),
                        init: global.init_expr().code().iter().map(From::from).collect(),
                    }
                })
                .collect()
        } else {
            Vec::new()
        };

        Ok(Module {
            types,
            functions,
            exports,
            tables,
            memory,
            globals,
        })
    }
}

#[derive(Error, Debug)]
pub enum ModuleError {
    #[error("unsupported version got verion '{0}' expected version '1'")]
    UnsupportedVersion(u32),
    #[error("constant out of range")]
    ConstantOutOfRange,
}

impl From<&elements::Internal> for Exports {
    fn from(src: &elements::Internal) -> Self {
        use elements::Internal;
        match src {
            &Internal::Function(v) => Exports::Function(FunctionIndex(v as usize)),
            &Internal::Table(v) => Exports::Table(TableIndex(v as usize)),
            &Internal::Memory(v) => Exports::Memory(MemoryIndex(v as usize)),
            &Internal::Global(v) => Exports::Global(GlobalIndex(v as usize)),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub(crate) enum ConstantInstruction {
    I32Const(i32),
    I64Const(i64),
    F32Const(u32),
    F64Const(u64),
    GetGlobal(u32),
    End,
}

impl From<&parity_wasm::elements::Instruction> for ConstantInstruction {
    fn from(src: &parity_wasm::elements::Instruction) -> Self {
        match src {
            elements::Instruction::GetGlobal(a) => ConstantInstruction::GetGlobal(*a),
            elements::Instruction::I32Const(a) => ConstantInstruction::I32Const(*a),
            elements::Instruction::I64Const(a) => ConstantInstruction::I64Const(*a),
            elements::Instruction::F32Const(a) => ConstantInstruction::F32Const(*a),
            elements::Instruction::F64Const(a) => ConstantInstruction::F64Const(*a),
            elements::Instruction::End => ConstantInstruction::End,
            v => panic!("non-const instruction: {:?}", v),
        }
    }
}

impl From<parity_wasm::elements::Instruction> for ConstantInstruction {
    fn from(src: parity_wasm::elements::Instruction) -> Self {
        ConstantInstruction::from(&src)
    }
}

impl From<&parity_wasm::elements::Instruction> for Instr {
    fn from(src: &parity_wasm::elements::Instruction) -> Self {
        src.to_owned().into()
    }
}
