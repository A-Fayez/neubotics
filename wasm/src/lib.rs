mod ast;
mod runtime;
mod values;

pub(crate) mod structure {
    pub(crate) mod instructions;
    pub(crate) mod types;
}

pub(crate) use structure::types;

pub use ast::Module;
pub use runtime::{Interpreter, Modules};
pub use values::Value;
