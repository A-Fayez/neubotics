use crate::{
    ast::{self, FunctionIndex, InstructionIndex},
    structure::instructions::{self, Instr},
    types,
    values::Value,
};
use std::collections::HashMap;
use thiserror::Error;

const PAGE_SIZE: usize = 65536;

#[derive(Default, Debug)]
pub struct Interpreter {
    modules: Modules,
    stack: Stack,
    result: Option<Vec<Value>>,
}

#[derive(Default, Debug)]
struct Stack(Vec<StackFrame>);

#[derive(Debug)]
pub struct StackFrame {
    locals: Box<[Value]>,
    module: ModuleIndex,
    function: FunctionIndex,
    instruction_pointer: InstructionIndex,
    blocks: Vec<Block>,
}

#[derive(Debug)]
pub struct Module {
    types: Vec<types::FuncType>,
    functions: Vec<ast::Function>,
    tables: Vec<Table>,
    memory: Vec<Memory>,
    globals: Vec<Global>,
    exports: HashMap<String, ast::Exports>,
}

#[derive(Default, Debug)]
pub struct Modules {
    modules: Vec<Module>,
    names: HashMap<String, ModuleIndex>,
}

type ComputationResult = Result<Vec<Value>, Trap>;

#[derive(Debug)]
pub struct Store {
    funcs: Vec<FuncInst>,
    tables: Vec<TableInst>,
    mems: Vec<MemInst>,
    globals: Vec<GlobalInst>,
}

#[derive(Debug, PartialEq, Clone, Copy)]
struct FuncAddr(usize);
#[derive(Debug, PartialEq, Clone, Copy)]
struct TableAddr(usize);
#[derive(Debug, PartialEq, Clone, Copy)]
struct MemAddr(usize);
#[derive(Debug, PartialEq, Clone, Copy)]
struct GlobalAddr(usize);

#[derive(Debug, PartialEq, Clone)]
struct ModuleInst {
    func_types: Vec<types::FuncType>,
    func_addrs: Vec<FuncAddr>,
    table_addrs: Vec<TableAddr>,
    mem_addrs: Vec<MemAddr>,
    global_addrs: Vec<GlobalAddr>,
    exports: Vec<ExportInst>,
}

#[derive(Debug, PartialEq, Clone)]
enum FuncInst {
    Module {
        func_type: types::FuncType,
        module: ModuleInst,
        code: Func,
    },
    Host {
        func_type: types::FuncType,
        host_code: (),
    },
}

#[derive(Debug, PartialEq, Clone)]
struct TableInst {
    elem: Vec<Option<FuncAddr>>,
    max: Option<u32>,
}
#[derive(Debug)]
pub struct Table {
    elements: Vec<Option<FunctionIndex>>,
    max: Option<u32>,
}

#[derive(Debug)]
struct MemInst {
    data: Vec<u8>,
    mac: Option<u32>,
}
#[derive(Debug)]
pub struct Memory {
    data: Vec<u8>,
    max: Option<u32>,
}

#[derive(Debug)]
struct GlobalInst {
    value: Value,
    mutable: types::Mut,
}

#[derive(Debug, Clone, Copy)]
pub struct Global {
    value: Value,
    is_mutable: bool,
}

type ExportInst = ();

type Func = ();
#[derive(Debug)]
pub struct Block {
    continuation: Continuation,
    return_type: types::Block,
    values: Vec<Value>,
}

#[derive(Debug)]
pub(crate) enum Continuation {
    Start(InstructionIndex),
    End,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub(crate) struct ModuleIndex(pub usize);

#[derive(Debug, PartialEq, Clone, Copy)]
struct Trap;
enum Control {
    Jump,
    JumpTo(InstructionIndex),
    Continue,
    Return,
}
use Control::*;

type InstructionResult = Result<Control, Trap>;

impl Modules {
    pub fn add_module(&mut self, name: impl Into<String>, module: Module) {
        let index = ModuleIndex(self.modules.len());
        self.modules.push(module);
        self.names.insert(name.into(), index);
    }

    fn get_index(&self, name: &str) -> Option<ModuleIndex> {
        Some(*self.names.get(name)?)
    }

    fn get(&self, index: ModuleIndex) -> &Module {
        &self.modules[index.0]
    }

    fn get_mut(&mut self, index: ModuleIndex) -> &mut Module {
        &mut self.modules[index.0]
    }
}

impl Module {
    pub(crate) fn function_index(&self, name: &str) -> Option<FunctionIndex> {
        self.exports.get(name).and_then(|&index| match index {
            ast::Exports::Function(i) => Some(i),
            _ => None,
        })
    }

    pub(crate) fn function(&self, index: FunctionIndex) -> &ast::Function {
        &self.functions[index.0]
    }

    pub(crate) fn function_types(&self, index: FunctionIndex) -> &types::FuncType {
        &self.types[self.function(index).type_index.0]
    }
}

impl Block {
    pub fn get_return_value(&self) -> Option<Value> {
        match self.return_type {
            types::Block::NoResult => None,
            types::Block::Value(value) => {
                let return_value = *self.values.last().expect("no value found on stack");
                assert!(
                    match value {
                        types::ValType::I32 => return_value.is_i32(),
                        types::ValType::I64 => return_value.is_i64(),
                        types::ValType::F32 => return_value.is_f32(),
                        types::ValType::F64 => return_value.is_f64(),
                    },
                    "wrong value type on top of the stack"
                );
                Some(return_value)
            }
        }
    }
}

impl Interpreter {
    pub fn add_import(&mut self, name: impl Into<String>) {}

    pub fn load_module(&mut self, name: impl Into<String>, module: &ast::Module) {
        assert!(!self.is_running());
        let mut globals: Vec<_> = module.globals.iter().map(Global::from).collect();
        let mut tables: Vec<_> = module.tables.iter().map(Table::from).collect();
        for (table_index, table) in module.tables.iter().enumerate() {
            assert_eq!(table_index, 0, "only one table currently supported");
            let offset = u32::from(
                table
                    .offset
                    .as_ref()
                    .map(|instructions| {
                        self.execute_constant_expression(
                            globals.as_slice(),
                            instructions.as_slice(),
                        )
                        .expect("no value on stack")
                    })
                    .unwrap_or(Value::I32(0)),
            );
            for (index, &function_index) in table.members.iter().enumerate() {
                tables[table_index].elements[offset as usize + index] = Some(function_index)
            }
        }
        let mut memory: Vec<_> = module.memory.iter().map(Memory::from).collect();
        for (memory_index, memory_segment) in module.memory.iter().enumerate() {
            assert_eq!(
                memory_index, 0,
                "only one memory section currently supported"
            );
            for data in &memory_segment.values {
                let offset = u32::from(
                    data.offset
                        .as_ref()
                        .map(|instructions| {
                            self.execute_constant_expression(
                                globals.as_slice(),
                                instructions.as_slice(),
                            )
                            .expect("no value on stack")
                        })
                        .unwrap_or(Value::I32(0)),
                );
                for (index, &data) in data.data.iter().enumerate() {
                    memory[memory_index].data[offset as usize + index] = data;
                }
            }
        }

        for (index, global) in module.globals.iter().enumerate() {
            globals[index].value = self
                .execute_constant_expression(globals.as_slice(), &global.init)
                .expect("no value on stack");
        }

        self.modules.add_module(
            name,
            Module {
                types: module.types.clone(),
                functions: module.functions.clone(),
                tables,
                memory,
                globals,
                exports: module.exports.clone(),
            },
        );
        //TODO support start function
    }

    pub fn call(
        &mut self,
        module_name: &str,
        function_name: &str,
        parameters: &[Value],
    ) -> Result<(), CallError> {
        let module_index =
            self.modules
                .get_index(module_name)
                .ok_or_else(|| CallError::ModuleNotFound {
                    module_name: module_name.to_string(),
                })?;

        let module = self.modules.get(module_index);

        let function_index =
            module
                .function_index(function_name)
                .ok_or_else(|| CallError::FunctionNotFound {
                    module_name: module_name.to_string(),
                    function_name: function_name.to_string(),
                })?;

        let actual_param_types = types::ResultType::from(
            parameters
                .iter()
                .map(|v| match v {
                    Value::I32(_) => types::ValType::I32,
                    Value::I64(_) => types::ValType::I64,
                    Value::F32(_) => types::ValType::F32,
                    Value::F64(_) => types::ValType::F64,
                })
                .collect::<Vec<_>>(),
        );
        let expected_param_types = &module.function_types(function_index).params;

        if expected_param_types != &actual_param_types {
            return Err(CallError::InvalidParameterTypes {
                got: actual_param_types,
                expected: expected_param_types.clone(),
            });
        }

        let function = &module.function(function_index);

        let mut locals = Vec::with_capacity(parameters.len() + function.locals.len());
        locals.extend_from_slice(parameters);
        locals.extend(function.locals.iter().map(|&l| Value::default_from_type(l)));

        self.stack.0.clear();
        self.stack.0.push(StackFrame {
            locals: locals.into_boxed_slice(),
            module: module_index,
            function: function_index,
            instruction_pointer: Default::default(),
            blocks: vec![Block {
                continuation: Continuation::End,
                return_type: module.function_types(function_index).to_block_results(),
                values: Vec::new(),
            }],
        });
        self.result = None;

        Ok(())
    }

    pub fn advance(&mut self, max_instructions: u64) -> u64 {
        if !self.is_running() {
            return max_instructions;
        }
        for i in 0..max_instructions {
            self.next();
            if self.result.is_some() {
                return max_instructions - i;
            }
        }
        0
    }

    pub fn results(&mut self) -> Option<&[Value]> {
        self.result.as_deref()
    }

    pub fn is_running(&self) -> bool {
        !self.stack.0.is_empty()
    }

    pub fn reset(&mut self) {
        self.stack.0.clear();
        self.result = None;
    }

    fn execute_constant_expression(
        &self,
        globals: &[Global],
        instructions: &[ast::ConstantInstruction],
    ) -> Option<Value> {
        use ast::ConstantInstruction::*;
        let mut stack = Vec::new();
        for instruction in instructions {
            match instruction {
                &I32Const(value) => stack.push(value.into()),
                &I64Const(value) => stack.push(value.into()),
                &F32Const(value) => stack.push((f32::from_bits(value)).into()),
                &F64Const(value) => stack.push((f64::from_bits(value)).into()),
                &GetGlobal(value) => stack.push(globals[value as usize].value),
                &End => return stack.pop(),
            }
        }
        panic!("ran out of instructions before finding 'end'");
    }

    fn i_drop(&mut self) -> InstructionResult {
        self.last_block_mut()
            .values
            .pop()
            .expect("no value on stack");
        Ok(Continue)
    }

    fn i_select(&mut self) -> InstructionResult {
        let stack = &mut self.last_block_mut().values;
        let condition = i32::from(stack.pop().expect("no value on stack"));
        let a = stack.pop().expect("no value on stack");
        let b = stack.pop().expect("no value on stack");
        assert_eq!(
            types::ValType::from(a),
            types::ValType::from(b),
            "values on stack are not the same type"
        );
        if condition == 0 {
            stack.push(a);
        } else {
            stack.push(b);
        }
        Ok(Continue)
    }

    fn i_get_local(&mut self, index: u32) -> InstructionResult {
        let frame = self.last_frame_mut();
        let block = frame.blocks.last_mut().expect("no block on stack");
        block.values.push(frame.locals[index as usize]);
        Ok(Continue)
    }

    fn i_set_local(&mut self, index: u32) -> InstructionResult {
        let value = self
            .last_block_mut()
            .values
            .pop()
            .expect("no value on stack");
        self.last_frame_mut().locals[index as usize] = value;
        Ok(Continue)
    }

    fn i_tee_local(&mut self, index: u32) -> InstructionResult {
        let &value = self
            .last_block_mut()
            .values
            .last()
            .expect("no value on stack");
        self.last_frame_mut().locals[index as usize] = value;
        Ok(Continue)
    }

    fn i_get_global(&mut self, index: u32) -> InstructionResult {
        let global = self.current_module().globals[index as usize];
        self.last_block_mut().values.push(global.value);
        Ok(Continue)
    }

    fn i_set_global(&mut self, index: u32) -> InstructionResult {
        let value = self
            .last_block_mut()
            .values
            .pop()
            .expect("no value on stack");

        let global = &mut self.current_module_mut().globals[index as usize];
        assert!(global.is_mutable, "global is not mutable");
        global.value = value;
        Ok(Continue)
    }

    fn i_load(
        &mut self,
        l_type: types::ValType,
        _align: u32,
        offset: u32,
        options: Option<instructions::LoadOption>,
    ) -> InstructionResult {
        let i = i32::from(self.last_block_mut().values.pop().expect("stack is empty"));

        let memory = &self.current_module_mut().memory[0];
        let ea = i as u32 + offset;
        let n = options
            .map(|o| o.bit_width())
            .unwrap_or_else(|| l_type.bit_width());
        if (ea + n as u32 / 8) as usize > memory.data.len() {
            return Err(Trap);
        }

        let value = memory
            .data
            .iter()
            .skip(ea as usize)
            .take(n as usize / 8)
            .enumerate()
            .fold(0, |acc, (i, b)| acc | ((*b as u64) << i as u64 * 8));

        let value = {
            use instructions::LoadOption::{S16, S32, S8};
            use types::ValType::{F32, F64, I32, I64};
            let value = match options {
                Some(S8) => (value as i8) as i64,
                Some(S16) => (value as i16) as i64,
                Some(S32) => (value as i32) as i64,
                _ => value as i64,
            };
            match l_type {
                I32 => Value::I32(value as i32),
                I64 => Value::I64(value as i64),
                F32 => Value::F32(f32::from_bits(value as u32)),
                F64 => Value::F64(f64::from_bits(value as u64)),
            }
        };

        self.last_block_mut().values.push(value);

        Ok(Continue)
    }

    fn i_store(
        &mut self,
        l_type: types::ValType,
        _align: u32,
        offset: u32,
        options: Option<instructions::StoreOption>,
    ) -> InstructionResult {
        let c = self.last_block_mut().values.pop().expect("stack is empty");
        assert_eq!(l_type, c.to_type());
        let n = options.map(|o| o.bit_width()).unwrap_or(c.bit_width());
        let c = match c {
            Value::I32(v) => v as u64,
            Value::I64(v) => v as u64,
            Value::F32(v) => v.to_bits() as u64,
            Value::F64(v) => v.to_bits(),
        };
        let i = i32::from(self.last_block_mut().values.pop().expect("stack is empty"));

        let memory = &mut self.current_module_mut().memory[0];
        let ea = i as u32 + offset;
        if (ea + n as u32 / 8) as usize > memory.data.len() {
            return Err(Trap);
        }

        let c = options
            .map(|o| c % 2u64.pow(o.bit_width() as u32))
            .unwrap_or(c);
        for (i, b) in memory
            .data
            .iter_mut()
            .skip(ea as usize)
            .take(n as usize / 8)
            .enumerate()
        {
            *b = ((c as u64 >> i as u64 * 8) & 0xFF) as u8;
        }

        Ok(Continue)
    }

    fn i_current_memory(&mut self, _: u8) -> InstructionResult {
        let sz = self.current_module_mut().memory[0].data.len() / PAGE_SIZE;
        self.last_block_mut().values.push((sz as i32).into());
        Ok(Continue)
    }

    fn i_grow_memory(&mut self, _: u8) -> InstructionResult {
        let n = i32::from(
            self.last_block_mut()
                .values
                .pop()
                .expect("no value on stack"),
        );
        let memory = &mut self.current_module_mut().memory[0];
        let sz = memory.data.len() / PAGE_SIZE;
        let err = -1i32;
        let len = n as usize + sz;
        if len > 1 << 16 {
            self.last_block_mut().values.push(err.into());
            return Ok(Continue);
        }
        if let Some(max) = memory.max {
            if (max as usize) < len {
                self.last_block_mut().values.push(err.into());
                return Ok(Continue);
            }
        }
        memory.data.extend((0..(n as usize * PAGE_SIZE)).map(|_| 0));
        self.last_block_mut().values.push((sz as i32).into());
        Ok(Continue)
    }

    fn i_const(&mut self, value: impl Into<Value>) -> InstructionResult {
        self.last_block_mut().values.push(value.into());
        Ok(Continue)
    }

    fn i_unop(&mut self, in_type: types::ValType, op: instructions::UnOp) -> InstructionResult {
        let values = &mut self.last_block_mut().values;
        let a = values.pop().expect("no value on stack");
        assert_eq!(a.to_type(), in_type);

        use instructions::{FUnOp, IUnOp, UnOp};
        values.push(match op {
            UnOp::IUnOp(op) => {
                let a = a.to_int().expect("stack value is not an int");
                match op {
                    IUnOp::Clz => a.clz(),
                    IUnOp::Ctz => a.ctz(),
                    IUnOp::Popcnt => a.popcnt(),
                }
            }
            UnOp::FUnOp(op) => {
                let a = a.to_float().expect("stack value is not a float");
                match op {
                    FUnOp::Abs => a.abs(),
                    FUnOp::Neg => a.neg(),
                    FUnOp::Sqrt => a.sqrt(),
                    FUnOp::Ceil => a.ceil(),
                    FUnOp::Floor => a.floor(),
                    FUnOp::Trunc => a.trunc(),
                    FUnOp::Nearest => a.nearest(),
                }
            }
        });
        Ok(Continue)
    }

    fn i_binop(&mut self, in_type: types::ValType, op: instructions::BinOp) -> InstructionResult {
        let values = &mut self.last_block_mut().values;
        let b = values.pop().expect("no value on stack");
        let a = values.pop().expect("no value on stack");
        assert_eq!(a.to_type(), in_type);
        assert_eq!(b.to_type(), in_type);

        use instructions::{BinOp, FBinOp, IBinOp};
        values.push(match op {
            BinOp::IBinOp(op) => {
                let a = a.to_int().expect("top of stack is not an int");
                let b = b.to_int().expect("top of stack is not an int");
                match op {
                    IBinOp::Add => a.add(b),
                    IBinOp::Sub => a.sub(b),
                    IBinOp::Mul => a.mul(b),
                    IBinOp::DivS => a.div_s(b),
                    IBinOp::DivU => a.div_u(b),
                    IBinOp::RemS => a.rem_s(b),
                    IBinOp::RemU => a.rem_u(b),
                    IBinOp::And => a.and(b),
                    IBinOp::Or => a.or(b),
                    IBinOp::Xor => a.xor(b),
                    IBinOp::Shl => a.shl(b),
                    IBinOp::ShrS => a.shr_s(b),
                    IBinOp::ShrU => a.shr_u(b),
                    IBinOp::Rotl => a.rotl(b),
                    IBinOp::Rotr => a.rotr(b),
                }
            }
            BinOp::FBinOp(op) => {
                let a = a.to_float().expect("top of stack is not a float");
                let b = b.to_float().expect("top of stack is not a float");
                match op {
                    FBinOp::Add => a.add(b),
                    FBinOp::Sub => a.sub(b),
                    FBinOp::Mul => a.mul(b),
                    FBinOp::Div => a.div(b),
                    FBinOp::Min => a.min(b),
                    FBinOp::Max => a.max(b),
                    FBinOp::Copysign => a.copysign(b),
                }
            }
        });
        Ok(Continue)
    }

    fn i_itestop(
        &mut self,
        in_type: types::IntType,
        op: instructions::ITestOp,
    ) -> InstructionResult {
        let values = &mut self.last_block_mut().values;
        let a = values
            .pop()
            .expect("no value on stack")
            .to_int()
            .expect("top of the stack is not an int");
        assert_eq!(a.to_type(), in_type);

        use instructions::ITestOp;
        values.push(match op {
            ITestOp::Eqz => a.eqz(),
        });
        Ok(Continue)
    }

    fn i_relop(&mut self, in_type: types::ValType, op: instructions::RelOp) -> InstructionResult {
        let values = &mut self.last_block_mut().values;
        let b = values.pop().expect("no value on stack");
        let a = values.pop().expect("no value on stack");
        assert_eq!(a.to_type(), in_type);
        assert_eq!(b.to_type(), in_type);

        use instructions::{FRelOp, IRelOp, RelOp};
        values.push(match op {
            RelOp::IRelOp(op) => {
                let a = a.to_int().expect("top of stack is not an int");
                let b = b.to_int().expect("top of stack is not an int");
                match op {
                    IRelOp::Eq => a.eq(b),
                    IRelOp::Ne => a.ne(b),
                    IRelOp::LtS => a.lt_s(b),
                    IRelOp::LtU => a.lt_u(b),
                    IRelOp::GtS => a.gt_s(b),
                    IRelOp::GtU => a.gt_u(b),
                    IRelOp::LeS => a.le_s(b),
                    IRelOp::LeU => a.le_u(b),
                    IRelOp::GeS => a.ge_s(b),
                    IRelOp::GeU => a.ge_u(b),
                }
            }
            RelOp::FRelOp(op) => {
                let a = a.to_float().expect("top of stack is not a float");
                let b = b.to_float().expect("top of stack is not a float");
                match op {
                    FRelOp::Eq => a.eq(b),
                    FRelOp::Ne => a.ne(b),
                    FRelOp::Lt => a.lt(b),
                    FRelOp::Gt => a.gt(b),
                    FRelOp::Le => a.le(b),
                    FRelOp::Ge => a.ge(b),
                }
            }
        });
        Ok(Continue)
    }

    fn i_cvtop(&mut self, op: instructions::CvtOp) -> InstructionResult {
        let values = &mut self.last_block_mut().values;
        let a = values.pop().expect("no value on stack");

        use instructions::CvtOp;
        use types::{FloatType as FT, IntType as IT};
        values.push(match op {
            CvtOp::I32WrapI64 => Value::I32(a.to_i64().unwrap() as i32),
            CvtOp::I64ExtendSI32 => Value::I64(i64::from(a.to_i32().unwrap())),
            CvtOp::I64ExtendUI32 => Value::I64(i64::from(a.to_i32().unwrap() as u32)),
            CvtOp::Trunc { from, to, signed } => match (from, to, signed) {
                (FT::F32, IT::I32, true) => Value::I32(a.to_f32().unwrap() as i32),
                (FT::F32, IT::I32, false) => Value::I32((a.to_f32().unwrap() as u32) as i32),
                (FT::F32, IT::I64, true) => Value::I64(a.to_f32().unwrap() as i64),
                (FT::F32, IT::I64, false) => Value::I64((a.to_f32().unwrap() as u64) as i64),
                (FT::F64, IT::I32, true) => Value::I32(a.to_f64().unwrap() as i32),
                (FT::F64, IT::I32, false) => Value::I32((a.to_f64().unwrap() as u32) as i32),
                (FT::F64, IT::I64, true) => Value::I64(a.to_f64().unwrap() as i64),
                (FT::F64, IT::I64, false) => Value::I64((a.to_f64().unwrap() as u64) as i64),
            },
            CvtOp::Convert { from, to, signed } => match (from, to, signed) {
                (IT::I32, FT::F32, true) => Value::F32(a.to_i32().unwrap() as f32),
                (IT::I32, FT::F32, false) => Value::F32((a.to_i32().unwrap() as u32) as f32),
                (IT::I32, FT::F64, true) => Value::F64(a.to_i32().unwrap() as f64),
                (IT::I32, FT::F64, false) => Value::F64((a.to_i32().unwrap() as u32) as f64),
                (IT::I64, FT::F32, true) => Value::F32(a.to_i64().unwrap() as f32),
                (IT::I64, FT::F32, false) => Value::F32((a.to_i64().unwrap() as u64) as f32),
                (IT::I64, FT::F64, true) => Value::F64(a.to_i64().unwrap() as f64),
                (IT::I64, FT::F64, false) => Value::F64((a.to_i64().unwrap() as u64) as f64),
            },
            CvtOp::F32DemoteF64 => Value::F32(a.to_f64().unwrap() as f32),
            CvtOp::F64PromoteF32 => Value::F64(a.to_f32().unwrap() as f64),
            CvtOp::I32ReinterpretF32 => {
                Value::I32(i32::from_ne_bytes(a.to_f32().unwrap().to_ne_bytes()))
            }
            CvtOp::I64ReinterpretF64 => {
                Value::I64(i64::from_ne_bytes(a.to_f64().unwrap().to_ne_bytes()))
            }
            CvtOp::F32ReinterpretI32 => {
                Value::F32(f32::from_ne_bytes(a.to_i32().unwrap().to_ne_bytes()))
            }
            CvtOp::F64ReinterpretI64 => {
                Value::F64(f64::from_ne_bytes(a.to_i64().unwrap().to_ne_bytes()))
            }
        });
        Ok(Continue)
    }

    fn i_block(&mut self, block: types::Block) -> InstructionResult {
        self.last_frame_mut().blocks.push(Block {
            continuation: Continuation::End,
            return_type: block,
            values: Vec::new(),
        });
        Ok(Continue)
    }

    fn i_loop(&mut self, block: types::Block) -> InstructionResult {
        let ip = self.last_frame().instruction_pointer;
        self.last_frame_mut().blocks.push(Block {
            continuation: Continuation::Start(ip),
            return_type: block,
            values: Vec::new(),
        });
        Ok(Continue)
    }

    fn i_if(&mut self, block: types::Block) -> InstructionResult {
        let stack = &mut self.last_block_mut().values;
        let condition = i32::from(stack.pop().expect("no value on stack"));

        self.last_frame_mut().blocks.push(Block {
            continuation: Continuation::End,
            return_type: block,
            values: Vec::new(),
        });

        if condition != 0 {
            Ok(Continue)
        } else {
            let code = &self.current_function().code;
            let mut acc = 0;
            let (pos, instruction) = code
                .iter()
                .enumerate()
                .skip(self.last_frame().instruction_pointer.0 + 1)
                .find(|(_, instruction)| {
                    acc += match instruction {
                        Instr::End => -1,
                        Instr::If(_) | Instr::Block(_) | Instr::Loop(_) => 1,
                        _ => 0,
                    };
                    (acc == 0 && *instruction == &Instr::Else)
                        || (acc < 0 && *instruction == &Instr::End)
                })
                .expect("could not find labeled end instruction");

            match instruction {
                Instr::Else => Ok(JumpTo(InstructionIndex(pos + 1))),
                Instr::End => Ok(JumpTo(InstructionIndex(pos))),
                _ => panic!("expected else or end instruction"),
            }
        }
    }

    fn i_else(&mut self) -> InstructionResult {
        let code = &self.current_function().code;
        let mut acc = 0;
        let (index, _) = code
            .iter()
            .enumerate()
            .skip(self.last_frame().instruction_pointer.0 + 1)
            .find(|(_, instruction)| {
                acc += match instruction {
                    Instr::End => -1,
                    Instr::If(_) | Instr::Block(_) | Instr::Loop(_) => 1,
                    _ => 0,
                };
                acc < 0 && *instruction == &Instr::End
            })
            .expect("could not find labeled end instruction");

        Ok(JumpTo(InstructionIndex(index)))
    }

    fn i_return(&mut self) -> InstructionResult {
        let mut frame = self.stack.0.pop().expect("no frame on stack");

        let return_value = frame
            .blocks
            .last_mut()
            .expect("no frame on stack")
            .values
            .pop();

        if let Some(return_value) = return_value {
            frame
                .blocks
                .first_mut()
                .expect("no frame on stack")
                .values
                .push(return_value);
        }

        let return_value = frame
            .blocks
            .first()
            .expect("no frame on stack")
            .get_return_value();

        match (self.stack.0.is_empty(), return_value) {
            (true, Some(return_value)) => self.result = Some(vec![return_value]),
            (true, None) => self.result = Some(Vec::new()),
            (false, Some(return_value)) => self.last_block_mut().values.push(return_value),
            _ => (),
        }

        Ok(Return)
    }

    fn i_call(&mut self, function_index: u32) -> InstructionResult {
        let module_index = self.last_frame().module;
        let function_index = FunctionIndex(function_index as usize);

        let module = &self.modules.get(module_index);
        let stack = &mut self.stack;

        let function_type = &module.function_types(function_index);
        let function = &module.function(function_index);

        let mut locals = Vec::with_capacity(function_type.params.len() + function.locals.len());
        locals.extend({
            let values = &mut stack
                .0
                .last_mut()
                .expect("no value on stack")
                .blocks
                .last_mut()
                .expect("no block on stack frame")
                .values;
            values.split_off(values.len() - function_type.params.len())
        });

        function_type
            .params
            .iter()
            .zip(locals.iter())
            .for_each(|(&param_type, &stack_value)| {
                assert_eq!(types::ValType::from(stack_value), param_type);
            });
        locals.extend(function.locals.iter().map(|&l| Value::default_from_type(l)));

        self.stack.0.push(StackFrame {
            locals: locals.into_boxed_slice(),
            module: module_index,
            function: function_index,
            instruction_pointer: Default::default(),
            blocks: vec![Block {
                continuation: Continuation::End,
                return_type: function_type.to_block_results(),
                values: Vec::new(),
            }],
        });
        Ok(Jump)
    }

    fn i_call_indirect(&mut self, type_index: u32, _: u8) -> InstructionResult {
        let index = i32::from(
            self.last_block_mut()
                .values
                .pop()
                .expect("no value on stack"),
        );

        let stack = &mut self.stack;
        let module_index = stack.0.last().expect("no frame on stack").module;
        let module = self.modules.get(module_index);
        let tab = &module.tables[0];
        let expected_function_type = &module.types[type_index as usize];

        if index as usize >= tab.elements.len() {
            return Err(Trap);
        }

        let function_index = tab.elements[index as usize].ok_or(Trap)?;

        let function = module.function(function_index);
        let actual_function_type = &module.types[function.type_index.0];
        if actual_function_type != expected_function_type {
            return Err(Trap);
        }

        let mut locals =
            Vec::with_capacity(actual_function_type.params.len() + function.locals.len());
        let parameters = {
            let values = &mut stack
                .0
                .last_mut()
                .expect("no value on stack")
                .blocks
                .last_mut()
                .expect("no block on stack frame")
                .values;
            values.split_off(values.len() - actual_function_type.params.len())
        };
        locals.extend(
            actual_function_type
                .params
                .iter()
                .zip(parameters.iter())
                .map(|(&p_type, &value)| {
                    assert_eq!(types::ValType::from(value), p_type);
                    value
                }),
        );
        locals.extend(function.locals.iter().map(|&l| Value::default_from_type(l)));

        self.stack.0.push(StackFrame {
            locals: locals.into_boxed_slice(),
            module: module_index,
            function: function_index,
            instruction_pointer: Default::default(),
            blocks: vec![Block {
                continuation: Continuation::End,
                return_type: actual_function_type.to_block_results(),
                values: Vec::new(),
            }],
        });
        Ok(Jump)
    }

    fn i_end(&mut self) -> InstructionResult {
        let block = self
            .last_frame_mut()
            .blocks
            .pop()
            .expect("no block on stack");
        let return_value = block.get_return_value();

        let control = if self.last_frame().blocks.is_empty() {
            self.stack.0.pop().expect("no frame on stack");
            Return
        } else {
            Continue
        };

        match (self.stack.0.is_empty(), return_value) {
            (true, Some(return_value)) => self.result = Some(vec![return_value]),
            (true, None) => self.result = Some(Vec::new()),
            (false, Some(return_value)) => self.last_block_mut().values.push(return_value),
            _ => (),
        }

        Ok(control)
    }

    fn i_br(&mut self, l: u32) -> InstructionResult {
        let return_value = self.last_block_mut().values.pop();

        for _ in 0..l {
            self.last_frame_mut()
                .blocks
                .pop()
                .expect("not enough blocks on the stack");
        }

        if let Some(value) = return_value {
            self.last_block_mut().values.push(value);
        }

        let code = &self.current_function().code;
        let ip = self.last_frame().instruction_pointer.0;
        Ok(JumpTo(match self.last_block().continuation {
            Continuation::Start(ip) => InstructionIndex(ip.0 + 1),
            Continuation::End => {
                let mut acc = l as i32;
                let (pos, _) = code[ip..]
                    .iter()
                    .enumerate()
                    .filter_map(|(i, instruction)| match instruction {
                        Instr::End => Some((i, -1)),
                        Instr::If(_) | Instr::Block(_) | Instr::Loop(_) => Some((i, 1)),
                        _ => None,
                    })
                    .find(|(_, v)| {
                        acc += v;
                        acc < 0
                    })
                    .expect("could not find labeled end instruction");
                InstructionIndex(ip + pos)
            }
        }))
    }

    fn i_br_if(&mut self, l: u32) -> InstructionResult {
        let stack = &mut self.last_block_mut().values;
        let condition = i32::from(stack.pop().expect("no value on stack"));

        if condition != 0 {
            self.i_br(l)
        } else {
            Ok(Continue)
        }
    }

    fn i_br_table(&mut self, table: &[u32], default: u32) -> InstructionResult {
        let stack = &mut self.last_block_mut().values;
        let index = i32::from(stack.pop().expect("no value on stack"));
        if let Some(&l) = table.get(index as usize) {
            self.i_br(l)
        } else {
            self.i_br(default)
        }
    }

    fn next(&mut self) {
        let instruction = self
            .current_function()
            .instruction(self.last_frame().instruction_pointer);
        //self.dbg_memory();
        //self.dbg_tables();
        //self.dbg_stack();
        //println!("## Executing instruction {:?}", &instruction);

        let result = match instruction {
            Instr::Unreachable => unimplemented!(),
            Instr::Nop => Ok(Continue),
            Instr::Const(v) => self.i_const(v),
            Instr::Block(block) => self.i_block(block),
            Instr::Loop(block) => self.i_loop(block),
            Instr::If(block) => self.i_if(block),
            Instr::Else => self.i_else(),
            Instr::End => self.i_end(),
            Instr::Br(l) => self.i_br(l),
            Instr::BrIf(l) => self.i_br_if(l),
            Instr::BrTable(table, default) => self.i_br_table(&table, default),
            Instr::Return => self.i_return(),
            Instr::Call(function_index) => self.i_call(function_index),
            Instr::CallIndirect(x, y) => self.i_call_indirect(x, y),
            Instr::Drop => self.i_drop(),
            Instr::Select => self.i_select(),
            Instr::GetLocal(local_index) => self.i_get_local(local_index),
            Instr::SetLocal(local_index) => self.i_set_local(local_index),
            Instr::TeeLocal(local_index) => self.i_tee_local(local_index),
            Instr::GetGlobal(global_index) => self.i_get_global(global_index),
            Instr::SetGlobal(global_index) => self.i_set_global(global_index),
            Instr::Load {
                l_type,
                align,
                offset,
                options,
            } => self.i_load(l_type, align, offset, options),
            Instr::Store {
                l_type,
                align,
                offset,
                options,
            } => self.i_store(l_type, align, offset, options),
            Instr::CurrentMemory(a) => self.i_current_memory(a),
            Instr::GrowMemory(pages) => self.i_grow_memory(pages),
            Instr::UnOp(t, op) => self.i_unop(t, op),
            Instr::BinOp(t, op) => self.i_binop(t, op),
            Instr::ITestOp(t, op) => self.i_itestop(t, op),
            Instr::RelOp(t, op) => self.i_relop(t, op),
            Instr::CvtOp(op) => self.i_cvtop(op),
        };

        match result {
            Ok(Jump) => (),
            Ok(JumpTo(index)) => *self.instruction_pointer_mut() = index,
            Ok(Return) => {
                if let Some(frame) = self.stack.0.last_mut() {
                    frame.instruction_pointer.increment();
                }
            }
            Ok(Continue) => self.instruction_pointer_mut().increment(),
            Err(err) => unimplemented!("unhandled trap: {:?}", err),
        }
    }

    fn last_frame(&self) -> &StackFrame {
        self.stack.0.last().expect("no frames on the stack")
    }

    fn last_frame_mut(&mut self) -> &mut StackFrame {
        self.stack.0.last_mut().expect("no frames on the stack")
    }

    fn last_block(&self) -> &Block {
        self.last_frame()
            .blocks
            .last()
            .expect("no blocks on the stack")
    }

    fn last_block_mut(&mut self) -> &mut Block {
        self.last_frame_mut()
            .blocks
            .last_mut()
            .expect("no blocks on the stack")
    }

    fn instruction_pointer_mut(&mut self) -> &mut InstructionIndex {
        &mut self.last_frame_mut().instruction_pointer
    }

    fn current_module(&self) -> &Module {
        self.modules.get(self.last_frame().module)
    }

    fn current_module_mut(&mut self) -> &mut Module {
        self.modules.get_mut(self.last_frame().module)
    }

    fn current_function(&self) -> &ast::Function {
        self.current_module().function(self.last_frame().function)
    }
}

#[derive(Error, Debug)]
pub enum CallError {
    #[error("module not found: {module_name}")]
    ModuleNotFound { module_name: String },
    #[error("function not found in module '{module_name}': {function_name}")]
    FunctionNotFound {
        module_name: String,
        function_name: String,
    },
    #[error(
        "supplied parameters do not match the function type got {got:?} expected {expected:?}"
    )]
    InvalidParameterTypes {
        got: types::ResultType,
        expected: types::ResultType,
    },
}

impl From<&ast::Table> for Table {
    fn from(src: &ast::Table) -> Self {
        Self {
            elements: vec![None; src.initial as usize],
            max: src.max,
        }
    }
}

impl From<&ast::Memory> for Memory {
    fn from(src: &ast::Memory) -> Self {
        Self {
            data: vec![0; src.initial as usize * PAGE_SIZE],
            max: src.max,
        }
    }
}

impl From<&ast::Global> for Global {
    fn from(src: &ast::Global) -> Self {
        Self {
            value: Value::default_from_type(src.value_type),
            is_mutable: src.is_mutable,
        }
    }
}

impl Interpreter {
    fn dbg_memory(&self) {
        let module = self.last_frame().module;
        print!("Memory {{");
        for memory in &self.modules.get(module).memory {
            print!(
                "({}) {:?}",
                memory.data.len() / PAGE_SIZE,
                &memory.data[..memory.data.len().min(20)]
            );
        }
        println!("}}");
    }

    fn dbg_tables(&self) {
        let module = self.last_frame().module;
        print!("Tables {{");
        for table in &self.modules.get(module).tables {
            print!(
                "[{}]",
                table
                    .elements
                    .iter()
                    .enumerate()
                    .map(|(i, t)| match t {
                        Some(v) => format!("{}:{}", i, v.0),
                        None => format!("{}:-", i),
                    })
                    .collect::<Vec<_>>()
                    .join(", ")
            );
        }
        println!("}}");
    }

    fn dbg_stack(&self) {
        println!("Stack {{");
        for frame in self.stack.0.iter() {
            println!(
                "  Frame mod {} func {} ip: {} locals {:?} {{",
                frame.module.0, frame.function.0, frame.instruction_pointer.0, frame.locals,
            );
            for block in frame.blocks.iter() {
                println!(
                    "    Block c{:?} r{:?} {{",
                    block.continuation, block.return_type
                );
                for value in block.values.iter() {
                    println!("      {:?}", value)
                }
                println!("    }}");
            }
            println!("  }}");
        }
        println!("}}");
    }
}
